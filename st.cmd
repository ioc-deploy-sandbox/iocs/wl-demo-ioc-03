#!/usr/bin/env iocsh.bash

# Required modules
## module_name,version
require(streamdevice)
require(ess)

# Environment variables
#
## Database macros
epicsEnvSet(LOCATION, "CSLab Test VM")
epicsEnvSet(IOCNAME,  "wl-demo-ioc-03")

##Load IOCSH

iocInit()

#dbl > "$(TOP)/$(IOC)_PVs.list"

